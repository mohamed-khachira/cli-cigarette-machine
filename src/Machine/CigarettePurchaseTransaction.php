<?php

declare(strict_types=1);

namespace App\Machine;

/**
 * Class CigarettePurchaseTransaction
 * @package App\Machine
 */
class CigarettePurchaseTransaction implements PurchaseTransactionInterface
{
    protected int $itemQuantity;
    protected float $paidAmount;


    public function __construct(int $itemQuantity, float $paidAmount)
    {
        $errors = "";
        if ($itemQuantity <= 0 || $paidAmount <= 0) {
            $errors = "Item quantity and/or paid amount should be bigger than 0";
        }
        if (!empty($errors)) {
            throw new \InvalidArgumentException($errors);
        }
        $this->itemQuantity = $itemQuantity;
        $this->paidAmount = $paidAmount;
    }

    public function getItemQuantity(): int
    {
        return $this->itemQuantity;
    }

    public function getPaidAmount(): float
    {
        return $this->paidAmount;
    }
}
