<?php

declare(strict_types=1);

namespace App\Machine;

/**
 * Class InsufficientMoneyException
 * @package App\Machine
 */
class InsufficientMoneyException extends \Exception
{
}
