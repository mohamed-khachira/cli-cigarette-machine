<?php

declare(strict_types=1);

namespace App\Machine;

/**
 * Class CigaretteMachine
 * @package App\Machine
 */
class CigaretteMachine implements MachineInterface
{
    const ITEM_PRICE = 4.99;
    // The 8 different euro coins
    const COINS = [0.01, 0.02, 0.05, 0.10, 0.20, 0.50, 1, 2];

    /**
     * Returns purchased Item
     *
     * @param PurchaseTransactionInterface $purchaseTransaction
     * @return PurchasedItemInterface
     */
    public function execute(PurchaseTransactionInterface $purchaseTransaction): PurchasedItemInterface
    {
        $cigarettePurchasedItem = new CigarettePurchasedItem($purchaseTransaction);
        if ($cigarettePurchasedItem->isInsufficientMoney()) {
            throw new InsufficientMoneyException("The amount is insufficient");
        }
        return $cigarettePurchasedItem;
    }
}
