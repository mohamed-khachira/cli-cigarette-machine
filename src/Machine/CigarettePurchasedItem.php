<?php

declare(strict_types=1);

namespace App\Machine;

/**
 * Class CigarettePurchasedItem
 * @package App\Machine
 */
class CigarettePurchasedItem implements PurchasedItemInterface
{

    private PurchaseTransactionInterface $purchaseTransaction;

    function __construct(PurchaseTransactionInterface $purchaseTransaction)
    {
        $this->purchaseTransaction = $purchaseTransaction;
    }

    /**
     * Returns item quantity
     *
     * @return integer
     */
    public function getItemQuantity(): int
    {
        return $this->purchaseTransaction->getItemQuantity();
    }

    /**
     * Returns total amount
     *
     * @return float
     */
    public function getTotalAmount(): float
    {
        return $this->purchaseTransaction->getItemQuantity() * CigaretteMachine::ITEM_PRICE;
    }

    /**
     * Returns coin combination for the change
     *
     * @return array
     */
    public function getChange(): array
    {
        $amountLeft = $this->purchaseTransaction->getPaidAmount() - $this->getTotalAmount();
        $final_result = [];
        $coins = CigaretteMachine::COINS;
        rsort($coins);
        foreach ($coins as $coin) {
            $number_of_coins = floor(round($amountLeft / $coin, 2));
            if ($number_of_coins > 0) {
                $final_result[] = [$coin, $number_of_coins];
                $amountLeft -= ($coin * $number_of_coins);
            }
        }
        return $final_result;
    }

    /**
     * Verfiy if there is enough money, returns true is total amount > pain amount, false if not
     *
     * @return boolean
     */
    public function isInsufficientMoney(): bool
    {
        if ($this->getTotalAmount() > $this->purchaseTransaction->getPaidAmount()) {
            return true;
        }
        return false;
    }
}
