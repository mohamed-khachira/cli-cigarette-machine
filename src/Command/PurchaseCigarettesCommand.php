<?php

declare(strict_types=1);

namespace App\Command;

use App\Machine\CigaretteMachine;
use App\Machine\CigarettePurchaseTransaction;
use App\Machine\InsufficientMoneyException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CigaretteMachine
 * @package App\Command
 */
class PurchaseCigarettesCommand extends Command
{
    /**
     * @return void
     */
    protected function configure()
    {
        $this->addArgument('packs', InputArgument::REQUIRED, "How many packs do you want to buy?");
        $this->addArgument('amount', InputArgument::REQUIRED, "The amount in euro.");
    }

    /**
     * @param InputInterface   $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $itemCount = (int) $input->getArgument('packs');
        $amount = (float) \str_replace(',', '.', $input->getArgument('amount'));
        try {
            $purchaseTransaction = new CigarettePurchaseTransaction($itemCount, $amount);
            $cigaretteMachine = new CigaretteMachine();
            $purchaseCigarette = $cigaretteMachine->execute($purchaseTransaction);

            $output->writeln("\nYou bought <info>" . $purchaseCigarette->getItemQuantity() . "</info> packs of cigarettes for <info>-" . $purchaseCigarette->getTotalAmount() . "€</info>, each for <info>-" . $cigaretteMachine::ITEM_PRICE . "€</info>. \n");
            $output->writeln('Your change is:');

            $table = new Table($output);
            $table
                ->setHeaders(array('Coins', 'Count'))
                ->setRows($purchaseCigarette->getChange());
            $table->render();
            return Command::SUCCESS;
        } catch (\InvalidArgumentException $e) {
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
            return Command::INVALID;
        } catch (InsufficientMoneyException $e) {
            $output->writeln(sprintf('<error>%s</error>', $e->getMessage()));
            return Command::INVALID;
        }
    }
}
