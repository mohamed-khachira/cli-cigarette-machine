## OOP Coding Challenge : CLI cigarette machine

Your product owner has given you the task of developing a
small CLI cigarette machine. The input should be the amount
of packs a potential customer could want and the amount he is
going to give. The price per cigarette pack is a static -4,99€.
You _dont_ have to think about currencies, there are only €!

The result should be printed on the screen with the count and
the total amount of the purchased packs as well as a table
which tells the customer in which coin combination he is going
to get his change.

Example:

```
╭─vicgey@limango:/home
╰─$ php bin/console purchase-cigarettes 2 10.00

You bought 2 packs of cigarettes for -9,98€, each for -4,99€.

Your change is:
+-------+-------+
| Coins | Count |
+-------+-------+
| 0.02  | 1     |
+-------+-------+
```

### Requirements

- Think of scenarios like "less money given than total cost of
  amount" and please use the predefined project structure
  (command,interfaces etc.)

## Built With

- PHP 7.4
- Composer

## Installation with docker & docker-compose

1- Clone Repository from GitLab

```bash
git clone https://gitlab.com/mohamed-khachira/cli-cigarette-machine.git
```

2- Move into the folder

```bash
cd cli-cigarette-machine
```

3- Start containers

```bash
make start
```

4- Install the dependencies

```bash
docker-compose run php composer install
```

5- Run the script

```bash
docker-compose run php bin/console purchase-cigarettes 2 10.00
```

6- Run the tests

```bash
docker-compose run php make test
```

## Installation without docker & docker-compose

1- Clone Repository from GitLab

```bash
git clone https://gitlab.com/mohamed-khachira/cli-cigarette-machine.git
```

2- Move into the folder

```bash
cd cli-cigarette-machine
```

3- Install the dependencies

```bash
composer install
```

4- Run the script

```bash
php bin/console purchase-cigarettes 2 10.00
```

5- Run the tests

```bash
make test
```
