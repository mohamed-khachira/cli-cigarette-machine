<?php

declare(strict_types=1);

namespace App\Tests\Machine;

use App\Machine\CigarettePurchaseTransaction;
use PHPUnit\Framework\TestCase;

class CigarettePurchaseTransactionTest extends TestCase
{
    public function testGetItemQuantityAndPaidAmount(): void
    {
        $itemQuantity = 2;
        $paidAmount = 10.00;
        $cigarettePurchaseTransaction = new CigarettePurchaseTransaction($itemQuantity, $paidAmount);
        self::assertIsInt($cigarettePurchaseTransaction->getItemQuantity(), "ItemQuantity is not of type int");
        self::assertIsFloat($cigarettePurchaseTransaction->getPaidAmount(), "PaidAmount is not of type int");
        self::assertEquals($itemQuantity, $cigarettePurchaseTransaction->getItemQuantity());
        self::assertEquals($paidAmount, $cigarettePurchaseTransaction->getPaidAmount());
    }

    public function testNegativeItemQuantityAndPaidAmount(): void
    {
        $itemQuantity = -2;
        $paidAmount = -10.00;
        $this->expectException(\InvalidArgumentException::class);
        new CigarettePurchaseTransaction($itemQuantity, $paidAmount);
    }
}
