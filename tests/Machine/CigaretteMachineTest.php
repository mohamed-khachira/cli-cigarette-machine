<?php

declare(strict_types=1);

namespace App\Tests\Machine;

use App\Machine\CigaretteMachine;
use App\Machine\CigarettePurchaseTransaction;
use App\Machine\InsufficientMoneyException;
use App\Machine\PurchasedItemInterface;
use PHPUnit\Framework\TestCase;

class CigaretteMachineTest extends TestCase
{
    /**
     * @dataProvider provideExactPrices
     */
    public function testExecuteWithExactPrice($itemQuantity, $paidAmount, $expectedChange): void
    {
        $cigarettePurchaseTransaction = new CigarettePurchaseTransaction($itemQuantity, $paidAmount);
        $cigaretteMachine = new CigaretteMachine();
        $purchaseCigarette = $cigaretteMachine->execute($cigarettePurchaseTransaction);
        self::assertInstanceOf(PurchasedItemInterface::class, $purchaseCigarette);
        self::assertEquals($purchaseCigarette->getItemQuantity(), $itemQuantity);
        self::assertEquals($purchaseCigarette->getTotalAmount(), CigaretteMachine::ITEM_PRICE * $itemQuantity);
        $this->assertEquals($expectedChange, $purchaseCigarette->getChange());
    }

    /**
     * @dataProvider provideExratPrices
     */
    public function testExecuteWithExtratMoney($itemQuantity, $paidAmount, $expectedChange): void
    {
        $cigarettePurchaseTransaction = new CigarettePurchaseTransaction($itemQuantity, $paidAmount);
        $cigaretteMachine = new CigaretteMachine();
        $purchaseCigarette = $cigaretteMachine->execute($cigarettePurchaseTransaction);
        self::assertInstanceOf(PurchasedItemInterface::class, $purchaseCigarette);
        self::assertEquals($purchaseCigarette->getItemQuantity(), $itemQuantity);
        self::assertEquals($purchaseCigarette->getTotalAmount(), CigaretteMachine::ITEM_PRICE * $itemQuantity);
        $this->assertEquals($expectedChange, $purchaseCigarette->getChange());
    }

    public function testExecuteWithInsufficientMoney(): void
    {
        $itemQuantity = 3;
        $paidAmount = 10.00;
        $cigarettePurchaseTransaction = new CigarettePurchaseTransaction($itemQuantity, $paidAmount);
        $cigaretteMachine = new CigaretteMachine();
        $this->expectException(InsufficientMoneyException::class);
        $purchaseCigarette = $cigaretteMachine->execute($cigarettePurchaseTransaction);
    }

    public function provideExactPrices(): array
    {
        return [
            ['itemQuantity' => 1, 'paidAmount' => CigaretteMachine::ITEM_PRICE, 'expectedChange' => []],
            ['itemQuantity' => 2, 'paidAmount' => CigaretteMachine::ITEM_PRICE * 2, 'expectedChange' => []],
            ['itemQuantity' => 3, 'paidAmount' => CigaretteMachine::ITEM_PRICE * 3, 'expectedChange' => []]
        ];
    }

    public function provideExratPrices(): array
    {
        return [
            ['itemQuantity' => 2, 'paidAmount' => 10.00, 'expectedChange' => [[0.02, 1]]],
            ['itemQuantity' => 2, 'paidAmount' => 20, 'expectedChange' => [[2, 5], [0.02, 1]]],
            ['itemQuantity' => 3, 'paidAmount' => 15, 'expectedChange' => [[0.02, 1], [0.01, 1]]]
        ];
    }
}
