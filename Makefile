.PHONY: test help

.DEFAULT_GOAL = help
DOCKER_COMPOSE=docker-compose
DOCKER_COMPOSE_EXEC=$(DOCKER_COMPOSE) exec
PHP_DOCKER_COMPOSE_EXEC=$(DOCKER_COMPOSE_EXEC) php

test: ## Run unit tests
	./vendor/bin/phpunit
## —— Docker 🐳  ———————————————————————————————————————————————————————————————
start:	## Start containers
	$(DOCKER_COMPOSE) up --build -d

stop:	## Stop containers
	$(DOCKER_COMPOSE) stop

rm:	stop ## Remove containers
	$(DOCKER_COMPOSE) rm -f

restart: rm start	## Restart containers

ssh-php:	## To get a bash shell in the container
	$(PHP_DOCKER_COMPOSE_EXEC) bash

## —— Others 🛠️️ ———————————————————————————————————————————————————————————————
help: ## List of commands
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
